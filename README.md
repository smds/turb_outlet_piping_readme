The turbine outlet piping connects the outlet of the turbine to the hot-side inputs of a paired set of heat exchangers. It consists of seven pipe segments and eight pipe fittings, including a flow split.

This sub-assembly is self-contained and does not require specifying any physical parameters.